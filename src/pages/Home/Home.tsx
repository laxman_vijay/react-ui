import React, { useState, useEffect } from 'react';
import './Home.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import CircularProgressBar from '../../shared/CircularProgressBar/CircularProgressBar';
import Avatar from '../../shared/Avatar/Avatar';
import Card from '../../shared/Card/Card';
import { IUserData, getUserData } from '../../core/api/user.api';
import Spinner from '../../shared/Spinner/Spinner';
import ActivityLogs from '../../shared/ActivityLogs/ActivityLogs';
import Chart from '../../shared/Chart/Chart';
import * as logger from 'loglevel';

export interface HomeProps {

}

const Home: React.FunctionComponent<HomeProps> = () => {

    const val = 80;
    let [userData, setUserData] = useState<IUserData>({
        'name': '',
        'objectivesCount': 0,
        'acheivedCount': 0,
        'atRiskCount': 0,
        'onTrackCount': 0
    });

    useEffect(() => {
        logger.trace("Home.tsx : useEffect : entering hook")
        getUserData().subscribe(data => {
            setUserData(data.data)
            logger.info("Home.tsx : useEffect : data retrieved from getUserData() : ", data)
        })
        logger.trace("Home.tsx : useEffect : entering hook")
    }, []);

    return (
        <div className="home">
            <div className="top-bar">
                <div className="items">
                    <FontAwesomeIcon icon={faQuestionCircle} className="icon" />
                    <CircularProgressBar value={val} />
                    <p className="text">Q3 2020</p>
                </div>
            </div>
            {userData.name === '' ? <Spinner /> :
                <>
                    <div className="name-bar">
                        <Avatar radius="3rem" name={userData.name} />
                        <p>{userData.name}</p>
                    </div>
                    <div className="cards">
                        <Card heading="Objectives" subHeading="Total number of objectives" count={userData.objectivesCount} />
                        <Card heading="Objectives Acheived" subHeading="Objectives over 70% progress" count={userData.acheivedCount} />
                        <Card heading="Objectives at risk" subHeading="Objectives below 20% progress" count={userData.atRiskCount} />
                        <Card heading="Objectives on track" subHeading="Objectives with 20% - 70% progress" count={userData.onTrackCount} />
                        <Card heading="Activity Log" height="30rem">
                            <Chart />
                        </Card>
                        <Card heading="Activity Log" height="30rem">
                            <ActivityLogs />
                        </Card>
                    </div>
                </>
            }
        </div>
    );
}

export default Home;