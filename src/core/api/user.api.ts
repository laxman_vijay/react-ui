import Axios from 'axios-observable';
import { AxiosObservable } from 'axios-observable/dist/axios-observable.interface';
import * as logger from 'loglevel';
import { local_test_url } from '../../environments/environment.dev';

export interface IUserData {
    name: string;
    objectivesCount: number;
    acheivedCount: number;
    atRiskCount: number;
    onTrackCount: number;
}

export interface IActivityLog {
    id: number;
    name: string;
    desc: string;
    date: string;
}

export function getUserData(): AxiosObservable<IUserData> {
    logger.trace("user.api.ts : getUserData : entering function")
    return Axios.get<IUserData>(`${local_test_url}/user`)
}

export function getActivityLog(): AxiosObservable<IActivityLog[]> {
    logger.trace("user.api.ts : getActivityLog : entering function")
    return Axios.get<IActivityLog[]>(`${local_test_url}/activities`)
}