import { IActivityLog } from "../api/user.api"

export const tActivity: IActivityLog = {
    'id': 1,
    'name': 'Kaavya Venkadesan',
    'desc': 'Added an Achievement on the key result "Clear the test" - Cleared the test',
    'date': '20 Jul 2020 10:50 PM'
}

export const tActivityLogs: IActivityLog[] = [
        {
            'id': 1,
            'name': 'Kaavya Venkadesan',
            'desc': 'Added an Achievement on the key result "Clear the test" - Cleared the test',
            'date': '20 Jul 2020 10:50 PM'
        },
        {
            'id': 2,
            'name': 'Caitlin Simms',
            'desc': 'Created a Key Result - Create Jira ticket template for data projects',
            'date': '20 Jul 2020 07:11 PM'
        },
        {
            'id': 3,
            'name': 'Ratnasri Panchumarthi',
            'desc': 'Added an Accomplishment - Certified ScrumMaster®',
            'date': '20 Jul 2020 04:14 PM'
        },
        {
            'id': 4,
            'name': 'Kaavya Venkadesan',
            'desc': 'Added an Achievement on the key result "Clear the test" - Cleared the test',
            'date': '20 Jul 2020 10:50 PM'
        }
]