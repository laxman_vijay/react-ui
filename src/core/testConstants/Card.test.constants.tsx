import { CardProps } from "../../shared/Card/Card";

export const tCard1: CardProps = {
    'heading': 'heading',
    'subHeading': 'subheading',
    'count': 2,
    'height': '5rem'

}

export const tCardCountColors = ['black', 'green', 'blue', 'orange', 'red', 'violet', 'gold']