import { AvatarProps } from "../../shared/Avatar/Avatar"

export const tAvatar1: AvatarProps = {
    'name': 'abc',
    'radius': '5rem'
}

export const tAvatar2: AvatarProps = {
    'name': 'ABC',
    'radius': '5rem'
}

export const tAvatarColors = ['black', 'green', 'blue', 'orange', 'red', 'violet', 'gold']