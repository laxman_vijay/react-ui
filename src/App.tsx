import React from 'react';
import './App.scss';
import { BrowserRouter } from 'react-router-dom';
import Navbar from './shared/Navbar/Navbar';
import Routes from './Routes';


function App() {
  return (
    <BrowserRouter>
      <Navbar/>
      <div className="App">
      <Routes/>
      </div>
    </BrowserRouter>
  );
}

export default App;
