import React from 'react';
import { shallow } from 'enzyme';
import Card from './Card';
import { tCard1, tCardCountColors } from '../../core/testConstants/Card.test.constants';

test('render card', () => {
    shallow(<Card heading={tCard1.heading}/>)
});

test('expect heading to be same', () => {
    let out = shallow(<Card heading={tCard1.heading}/>)
    expect(out.find('.heading').text()).toEqual(tCard1.heading)
})

test('expect subheading if it is passed', () => {
    let out = shallow(<Card heading={tCard1.heading} subHeading={tCard1.subHeading}/>)
    expect(out.find('.sub-heading').text()).toEqual(tCard1.subHeading)
})

test('expect no subheading if it is not passed', () => {
    let out = shallow(<Card heading={tCard1.heading}/>)
    expect(out.find('.sub-heading')).toEqual({})
})

test('expect no count if it is not passed', () => {
    let out = shallow(<Card heading={tCard1.heading}/>)
    expect(out.find('.count')).toEqual({})
})

test('expect count if it is passed', () => {
    let out = shallow(<Card heading={tCard1.heading} count={tCard1.count}/>)
    expect(out.find('.count').text()).toEqual(tCard1.count?.toString())
})

test('expect count color to match the colors', () => {
    let out = shallow(<Card heading={tCard1.heading} count={tCard1.count}/>)
    expect(tCardCountColors.includes(out.find('.count').props().style.color)).toBeTruthy()
})

test("render children if passed", () => {
    let out = shallow(<Card heading={tCard1.heading} count={tCard1.count}>abc</Card>)
    expect(out.find('.card').text().includes('abc')).toBeTruthy()
})