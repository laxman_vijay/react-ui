import React, { FunctionComponent } from 'react';
import './Card.scss';

export interface CardProps {
    heading: string;
    subHeading?: string;
    count?: number;
    height?: string;
}

const Card: React.FunctionComponent<CardProps> = (props) => {

    const colors = ['black', 'green', 'blue', 'orange', 'red', 'violet']

    return (
        <div className="card" style={{ height: props.height }}>
            <p className="heading">{props.heading}</p>
            {props.subHeading ? <p className="sub-heading">{props.subHeading}</p> : ''}
            {props.count === undefined ? '' : <p className="count" style={{ color: colors[Math.floor(Math.random() * colors.length)] }}>{props.count}</p>}
            {props.children}
        </div>
    );
}

export default Card;