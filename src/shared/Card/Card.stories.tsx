import React from 'react';
import Card from './Card';
import { tCard1 } from '../../core/testConstants/Card.test.constants';
import Chart from '../Chart/Chart';
import ActivityLogs from '../ActivityLogs/ActivityLogs';
import '../../index.scss';

export default { title: 'Card' };

export const defaultState = () => <Card heading={tCard1.heading} subHeading={tCard1.subHeading} count={tCard1.count}/>

export const withoutSubHeading = () => <Card heading={tCard1.heading} count={tCard1.count}/>

export const withoutCount = () => <Card heading={tCard1.heading}/>

export const withChart = () => <Card heading={tCard1.heading}><Chart/></Card>

export const withActivityLog = () => <Card heading={tCard1.heading}><ActivityLogs/></Card>