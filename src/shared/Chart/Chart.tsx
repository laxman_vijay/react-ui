import React from 'react';
import { Line } from 'react-chartjs-2';

export interface ChartProps {

}

const Chart: React.FunctionComponent<ChartProps> = () => {
  const state = {
    labels: ['January', 'February', 'March',
      'April', 'May'],
    datasets: [
      {
        label: 'Score',
        borderColor: 'rgba(0,0,0,1)',
        borderWidth: 2,
        data: [65, 59, 80, 81, 56]
      }
    ]
  }
  return (
    <Line
      data={state}
      options={{
        title: {
          display: true,
          text: 'Average Score',
          fontSize: 20
        },
        legend: {
          display: true,
          position: 'right'
        }
      }}
    />
  );
}

export default Chart;