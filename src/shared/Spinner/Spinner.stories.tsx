import React from 'react';
import Spinner from './Spinner';
import '../../index.scss';

export default { title: 'Spinner' };

export const defaultState = () => <Spinner/>