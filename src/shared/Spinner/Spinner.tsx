import React from 'react';
import './Spinner.scss';

export interface SpinnerProps {

}

const Spinner: React.FunctionComponent<SpinnerProps> = () => {
    return (
        <div className="loader"></div>
    );
}

export default Spinner;