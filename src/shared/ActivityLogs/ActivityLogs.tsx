import React, { useState, useEffect } from 'react';
import './ActivityLogs.scss';
import { IActivityLog, getActivityLog } from '../../core/api/user.api';
import Spinner from '../Spinner/Spinner';
import Activity from '../Activity/Activity';
import * as logger from 'loglevel';
logger.setLevel("TRACE")

export interface ActivityLogsProps {

}

const ActivityLogs: React.FunctionComponent<ActivityLogsProps> = () => {

    let [activityData, setActivityData] = useState<IActivityLog[]>();
    let [isLoading, setIsLoading] = useState(true);


    useEffect(() => {
        logger.trace("ActivityLogs.tsx : useEffect : entering hook")
        getActivityLog().subscribe(data => {
            setActivityData(data.data)
            setIsLoading(false);
            logger.info("ActivityLogs.tsx : useEffect : data retrieved from getActivityLog() : ", data)
        })
        logger.trace("ActivityLogs.tsx : useEffect : exiting hook")
    }, []);

    return (
        <div className="activity-logs">
            {
                !isLoading ?
                    activityData.map(x =>
                        <Activity name={x.name} desc={x.desc} date={x.date} key={x.id} />
                    ) :
                    <Spinner />
            }
        </div>
    );
}

export default ActivityLogs;