import React from 'react';
import { render } from '@testing-library/react';
import Navbar from './Navbar';
import { shallow } from 'enzyme';

test('renders navbar', () => {
  render(<Navbar/>);
})

test('expect one item to be active', () => {
  let out = shallow(<Navbar/>)
  expect(out.find('.item').find('.active').getElements().length).toEqual(1)
})
