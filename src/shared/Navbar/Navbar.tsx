import React from 'react';
import './Navbar.scss';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faDesktop, faStar, faTrophy, faCode, faUser, faUserFriends, faFlag, faCertificate, faSearch, faPowerOff } from '@fortawesome/free-solid-svg-icons';
import logo from './logo.svg';

export interface NavbarProps {

}

const Navbar: React.FunctionComponent<NavbarProps> = () => {
     return (
          <div className="navbar">
               <div className="logo">
                    <img src={logo} alt="" className="icon" />
                    <p>OKR</p>
               </div>
               <div className="nav-items">
                    <div className="item-set">
                         <p>Me</p>
                         <div className="item active">
                              <FontAwesomeIcon icon={faDesktop} className="icon" />
                              <p className="name">Dashboard</p>
                         </div>
                         <div className="item">
                              <FontAwesomeIcon icon={faStar} className="icon" />
                              <p className="name">Objectives</p>
                         </div>
                         <div className="item">
                              <FontAwesomeIcon icon={faTrophy} className="icon" />
                              <p className="name">Accomplishments</p>
                         </div>
                         <div className="item">
                              <FontAwesomeIcon icon={faCode} className="icon" />
                              <p className="name">Skills</p>
                         </div>
                    </div>
                    <div className="item-set">
                         <p>General</p>
                         <div className="item">
                              <FontAwesomeIcon icon={faUser} className="icon" />
                              <p className="name">People</p>
                         </div>
                         <div className="item">
                              <FontAwesomeIcon icon={faStar} className="icon" />
                              <p className="name">Objectives</p>
                         </div>
                         <div className="item">
                              <FontAwesomeIcon icon={faUserFriends} className="icon" />
                              <p className="name">Teams</p>
                         </div>
                    </div>
                    <div className="item-set">
                         <p>Certifications</p>
                         <div className="item">
                              <FontAwesomeIcon icon={faDesktop} className="icon" />
                              <p className="name">Dashboard</p>
                         </div>
                         <div className="item">
                              <FontAwesomeIcon icon={faFlag} className="icon" />
                              <p className="name">Achieved</p>
                         </div>
                         <div className="item">
                              <FontAwesomeIcon icon={faCertificate} className="icon" />
                              <p className="name">My Certifications</p>
                         </div>
                    </div>
                    <div className="item-set">
                         <p>Search</p>
                         <div className="item">
                              <FontAwesomeIcon icon={faSearch} className="icon" />
                              <p className="name">Search</p>
                         </div>
                    </div>
                    <div className="item-set">
                         <div className="item">
                              <FontAwesomeIcon icon={faPowerOff} className="icon" />
                              <p className="name">Logout</p>
                         </div>
                    </div>
               </div>
          </div>
     );
}

export default Navbar;