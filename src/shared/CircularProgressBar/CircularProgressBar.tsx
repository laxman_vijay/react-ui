import React from 'react';
import './CircularProgressBar.scss';
import * as logger from 'loglevel';

export interface CircularProgressBarProps {
    value: number;
}

const CircularProgressBar: React.FunctionComponent<CircularProgressBarProps> = () => {

    const canvasRef = React.useRef<HTMLCanvasElement>(null);
    const [ctx, setContext] = React.useState<CanvasRenderingContext2D | null>(null);

    React.useEffect(() => {
        logger.trace("CircularProgressBar.tsx : useEffect : entering hook")
        if (canvasRef.current) {
            const renderCtx = canvasRef.current.getContext('2d');
            if (renderCtx) {
                logger.debug("CircularProgressBar.tsx : useEffect : got canvas 2d context")
                setContext(renderCtx);
            }
        }
        if (ctx) {
            logger.debug("CircularProgressBar.tsx : useEffect : begin to draw")
            ctx.beginPath();
            ctx.arc(12, 12, 11, 0, Math.PI * 2);
            ctx.stroke();
            logger.debug("CircularProgressBar.tsx : useEffect : end to draw")
        }
        logger.trace("CircularProgressBar.tsx : useEffect : entering hook")
    }, [ctx]);

    return (
        <div className="outer">
            <canvas id="canvas" ref={canvasRef} width="28" height="28"></canvas>
        </div>
    );
}

export default CircularProgressBar;