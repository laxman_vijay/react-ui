import React from 'react';
import CircularProgressBar from './CircularProgressBar';
import '../../index.scss';

export default { title: 'Circular Progress Bar' };

export const defaultState = () => <CircularProgressBar value={80}/>