import React from 'react';
import './Activity.scss';
import Avatar from '../Avatar/Avatar';

export interface ActivityProps {
    name: string;
    desc: string;
    date: string;
}

const Activity: React.FunctionComponent<ActivityProps> = (props) => {

    return (
        <div className="activity">
            <Avatar radius="2.2rem" name={props.name} />
            <div className="text">
                <p className="first"><b>{props.name}</b> {props.desc}</p>
                <p className="second">{props.date}</p>
            </div>
        </div>
    );
}

export default Activity;