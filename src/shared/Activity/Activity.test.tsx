import React from 'react';
import Activity from './Activity';
import { render } from '@testing-library/react';
import { shallow } from 'enzyme';
import { tActivity } from '../../core/testConstants/Activity.test.constants';

test('avatar in the activity renders first letter', () => {
    let { getByText } = render(<Activity name={tActivity.name} desc={tActivity.desc} date={tActivity.date}/>)
    let text = getByText('K')
    expect(text).toBeInTheDocument()
});

test('boldness of name', () => {
    let el = shallow(<Activity name={tActivity.name} desc={tActivity.desc} date={tActivity.date}/>)
    expect(el.find('.first').html().includes('<b>')).toBeTruthy()
})