import React from 'react';
import Activity from './Activity';
import { tActivity } from '../../core/testConstants/Activity.test.constants';
import '../../index.scss';

export default { title: 'Activity' };

export const defaultState = () => <Activity name={tActivity.name} desc={tActivity.desc} date={tActivity.date}/>