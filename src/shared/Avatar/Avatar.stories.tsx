import React from 'react';
import Avatar from './Avatar';
import { tAvatar1 } from '../../core/testConstants/Avatar.test.constants';
import '../../index.scss';

export default { title: 'Avatar' };

export const defaultState = () => <Avatar name={tAvatar1.name} radius={tAvatar1.radius}/>