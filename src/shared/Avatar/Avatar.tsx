import React from 'react';
import './Avatar.scss';

export interface AvatarProps {
    radius: string;
    name: string;
}

const Avatar: React.SFC<AvatarProps> = (props) => {

    const colors = ['black', 'green', 'blue', 'orange', 'red', 'violet', 'gold']

    return (
        <div className="circle" style={{ height: props.radius, width: props.radius, backgroundColor: colors[Math.floor(Math.random() * colors.length)] }}>
            <div>{props.name.charAt(0).toUpperCase()}</div>
        </div>
    );
}

export default Avatar;