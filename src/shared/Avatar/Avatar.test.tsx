import React from 'react';
import { shallow } from 'enzyme';
import Avatar from './Avatar';
import { tAvatar1, tAvatar2, tAvatarColors } from '../../core/testConstants/Avatar.test.constants';

test('render avatar', () => {
    shallow(<Avatar name={tAvatar1.name} radius={tAvatar1.radius} />)
});

test('renders first letter in capital given name is lowercase (positive)', () => {
    let out = shallow(<Avatar name={tAvatar1.name} radius={tAvatar1.radius} />)
    expect(out.find('.circle').childAt(0).html().includes('A')).toBeTruthy()
})

test('renders first letter in capital given name is lowercase (negative)', () => {
    let out = shallow(<Avatar name={tAvatar1.name} radius={tAvatar1.radius} />)
    expect(out.find('.circle').childAt(0).html().includes('a')).toBeFalsy()
})

test('renders first letter in capital given name is uppercase (positive)', () => {
    let out = shallow(<Avatar name={tAvatar2.name} radius={tAvatar2.radius} />)
    expect(out.find('.circle').childAt(0).html().includes('A')).toBeTruthy()
})

test('renders first letter in capital given name is uppercase (negative)', () => {
    let out = shallow(<Avatar name={tAvatar2.name} radius={tAvatar2.radius} />)
    expect(out.find('.circle').childAt(0).html().includes('a')).toBeFalsy()
})

test('expect radius to be same', () => {
    let out = shallow(<Avatar name={tAvatar1.name} radius={tAvatar1.radius} />)
    expect(out.props().style.height).toEqual(tAvatar1.radius)
    expect(out.props().style.width).toEqual(tAvatar1.radius)
})

test('expect colors to be one of the given', () => {
    let out = shallow(<Avatar name={tAvatar1.name} radius={tAvatar1.radius} />)
    expect(tAvatarColors.includes(out.props().style.backgroundColor)).toBeTruthy()
})